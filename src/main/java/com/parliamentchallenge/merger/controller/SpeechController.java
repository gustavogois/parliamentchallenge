package com.parliamentchallenge.merger.controller;

import com.parliamentchallenge.merger.service.ServicesNames;
import com.parliamentchallenge.merger.dto.SpeechesDTO;
import com.parliamentchallenge.merger.service.RetrieveSpeechesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@RestController
public class SpeechController {

    @Autowired
    RetrieveSpeechesService retrieveSpeechesService;

    public SpeechController(RetrieveSpeechesService retrieveSpeechesService) {
        this.retrieveSpeechesService = retrieveSpeechesService;
    }

    @RequestMapping(ServicesNames.ROOT)
    public String index() {

        return "Let the games begin!";
    }

    @GetMapping(ServicesNames.RETRIEVE_LAST_TEN_SPEECHES)
    public SpeechesDTO retrieveLastTenSpeeches() throws IOException {

        return retrieveSpeechesService.retrieveLastTenSpeeches();
    }
}
