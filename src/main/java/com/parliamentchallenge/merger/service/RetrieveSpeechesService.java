package com.parliamentchallenge.merger.service;

import com.parliamentchallenge.merger.exception.SpeechesNotFoundException;
import com.parliamentchallenge.merger.mapper.Mapper;
import com.parliamentchallenge.merger.service.model.Member;
import com.parliamentchallenge.merger.service.dao.SpeechDAO;
import com.parliamentchallenge.merger.dto.SpeechesDTO;
import com.parliamentchallenge.merger.service.model.Speech;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.*;

@Component
public class RetrieveSpeechesService {

    @Autowired
    SpeechDAO speechDao;

    public SpeechesDTO retrieveLastTenSpeeches() throws IOException {

        List<Speech> speeches = speechDao.loadTenLastSpeeches();

        // I prefer not to use stream in this case to a better legibility
        Map<String, Member> membersSpeech = new HashMap();
        for(Speech speech : speeches) {
            Member memberSpeech = speechDao.findMemberSpeech(speech.getStakeholderId());
            membersSpeech.put(speech.getSpeechId(), memberSpeech);
        }

        SpeechesDTO speechesResponseDTO = new SpeechesDTO();
        speechesResponseDTO.setSpeechesDTOS(Mapper.speechesAndMembersToSpeechesDTO(speeches, membersSpeech));

        if(CollectionUtils.isEmpty(speechesResponseDTO.getSpeechesDTOS())) {
            throw new SpeechesNotFoundException("Any speech founded.");
        }

        return speechesResponseDTO;

    }
}
