package com.parliamentchallenge.merger.service;

public interface ServicesNames {
    String ROOT = "/";
    String RETRIEVE_LAST_TEN_SPEECHES = "/retrieveLastTenSpeeches";
}
