package com.parliamentchallenge.merger.util;

import java.util.Optional;

public class MemberHelper {
    public static String removeTitle(String speaker) {

        final String TITLE = "Justitie- och migrationsministern ";

        return speaker != null ? speaker.replace(TITLE, "") : "";
    }
}
