package com.parliamentchallenge.merger.controller;

import com.parliamentchallenge.merger.dto.SpeechesDTO;
import com.parliamentchallenge.merger.service.RetrieveSpeechesService;
import com.parliamentchallenge.merger.service.ServicesNames;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class SpeechControllerTest {

    SpeechController speechController;

    @Mock
    RetrieveSpeechesService retrieveSpeechesService;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        speechController = new SpeechController(retrieveSpeechesService);
    }

    @Test
    public void index() throws Exception {

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(speechController).build();
        mockMvc.perform(get(ServicesNames.ROOT))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Let the games begin!")));
    }

}